#Laravel and Scotchbox Recipe

Most people default to Homestead as their go-to box when using Vagrant and Laravel.  While there are a lot of awesome things about Homestead, I wanted a little more control and a little less YAML.  

#Scotchbox
Scotchbox is a barebones LAMP Vagrant Box created by the guys at scotch.io.  I have used Scotchbox for other development and really liked its simplicity.  The one thing I didn't like, was the configuration of the 'public' folder when you clone their entire repo (https://github.com/scotch-io/scotch-box)

More info here: https://box.scotch.io/   

##Enough backstory
1. Make sure to download Virtual Box and Vagrant
2. Clone this repo on your local machine.
3. Run 'vagrant up'
4. Check your browser at 'http://192.168.33.10/'

##That's all there is to it
It is a simple Vagrantfile and provisioning script (script.sh).  This allows you to do anything you want from there.

##Common Troubleshooting
The most common issue I ran across when running this on different Operating Systems with different versions of Virtual Box and Vagrant was the 'Guest Additions' error.  There are a few different flavors of the error, but after the 'vagrant up' command you see something like this "GuestAdditions versions on your host (4.3.10) and guest (4.2.16) do not match.
 * Stopping VirtualBox Additions
   ...done."

Solution:  install the vagrant guest additions plugin and run again (https://github.com/dotless-de/vagrant-vbguest)
